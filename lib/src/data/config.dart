import 'package:flutter/material.dart';

class Config {
  /* Images Dir */
  static const String imageDir = "assets/images";

  /* Default Logo Application*/
  static Image logo =
      Image.asset("$imageDir/logo.png", height: 150, width: 150);

  static List language = [
    {"value": "hr", "name": "Hrvatski", "subtitle": "Croatian"},
    {"value": "en", "name": "Engleski", "subtitle": "English"},
    {"value": "es", "name": "Španjolski", "subtitle": "Español"},
    {"value": "fr", "name": "Francuski", "subtitle": "Français"},
    {"value": "de", "name": "Njemački", "subtitle": "Deutsche"},
    {"value": "it", "name": "Talijanski", "subtitle": "Italiano"},
    {"value": "tr", "name": "Turski", "subtitle": "Turc"},
    {"value": "ru", "name": "Ruski", "subtitle": "русский язык"}
  ];
}
